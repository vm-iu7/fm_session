#!/usr/bin/env bash

APP_NAME=fm_session
USER=findmovie
GROUP=findmovie

echo $USER
echo $GROUP

function is_installed {
  if yum list installed "$@" >/dev/null 2>&1; then
    true
  else
    false
  fi
}

function install_nginx {
    echo "INFO: installing nginx..."

    yum install -y epel-release
    yum install -y nginx
    systemctl enable nginx

    if ! systemctl is-enabled nginx
    then
        echo "ERROR: failed to install nginx - is-enabled is false after installation"
        exit 1
    fi

    if ! systemctl start nginx; then
        echo "WARNING: Failed to start nginx"
    fi
}

function check_and_install_nginx {
    echo "INFO: checking if nginx installed"

    if ! is_installed nginx
    then
        echo "INFO: nginx is not installed. trying to install..."
        install_nginx
    fi

    /bin/cp ~/target_configs/nginx.conf /etc/nginx/
    /bin/cp ~/target_configs/front.nginx.conf /etc/nginx/conf.d/

    if ! systemctl is-active nginx; then
        echo "WARNING: nginx is not active, trying to start"
        systemctl start nginx
    else
        echo "INFO: reloading nginx"
        systemctl reload nginx
    fi

    if ! systemctl is-active nginx; then
        echo "ERROR: failed to start nginx: is-active is false"
        exit 1
    fi
}

function set_permissions {
    echo "INFO: setting permissions"

    chmod -R ug+rw /var/run/$APP_NAME
    chown -R $USER /var/run/$APP_NAME
    chown -R :$GROUP /var/run/$APP_NAME

    chmod -R ug+rw /usr/share/$APP_NAME
    chown -R $USER /usr/share/$APP_NAME
    chown -R :$GROUP /usr/share/$APP_NAME

    chown -R ug+rw /var/log/$APP_NAME
    chown -R $USER /var/log/$APP_NAME
    chown -R :$GROUP /var/log/$APP_NAME
}

function install_app {
    echo "INFO: installing $APP_NAME"
    if ! yum install -y $APP_NAME
    then
        echo "ERROR: Failed to install $APP_NAME"
        exit 1
    fi
}

function check_and_update_app {
    echo "INFO: checking if $APP_NAME is installed"
    if ! is_installed $APP_NAME
    then
        echo "INFO: $APP_NAME is not installed"
        install_app
    else
        echo "INFO: $APP_NAME is installed. upgrading.."
        if ! yum update -y $APP_NAME
        then
            echo "ERROR: Failed to upgrade $APP_NAME"
            exit 1
        fi
    fi

    set_permissions

    systemctl restart $APP_NAME

    if ! systemctl is-active $APP_NAME
    then
        echo "ERROR: Failed to run $APP_NAME - is-active is false"
        exit 1
    fi
}

function update_static {
    echo "INFO: updating static"

    if ! [ -d "/www" ]; then
        echo "/www not exists.. creating"
        mkdir /www
    fi

    if ! [ -d "/www/assets" ]; then
        echo "/www/assets not exists.. creating"
        mkdir /www/assets
    fi

    /bin/cp -rf ~/public/* /www/assets/

    chmod -R ug+rx /www
    chown -R nginx /www
    chown -R :nginx /www
}

yum install -y java-1.8.0-openjdk

/bin/cp ~/target_configs/ci.repo /etc/yum.repos.d/ci.repo

yum clean metadata

check_and_install_nginx
update_static
check_and_update_app