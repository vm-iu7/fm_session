name := """fm_session"""

version := "0.1.0"

lazy val root = (project in file(".")).enablePlugins(PlayScala, RpmPlugin)

scalaVersion := "2.11.6"

libraryDependencies ++= Seq(
  cache,
  ws,
  specs2 % Test,

  "org.scala-lang.modules" %% "scala-async" % "0.9.5",

  "mysql" % "mysql-connector-java" % "5.1.36",

  "net.debasishg" %% "redisclient" % "3.0",

  "com.typesafe.play" %% "play-slick" % "1.1.1",
  "com.typesafe.play" %% "play-slick-evolutions" % "1.1.1",

  "com.github.t3hnar" %% "scala-bcrypt" % "2.5"
)

resolvers += "scalaz-bintray" at "http://dl.bintray.com/scalaz/releases"

// Play provides two styles of routers, one expects its actions to be injected, the
// other, legacy style, accesses its actions statically.
routesGenerator := InjectedRoutesGenerator

javaOptions in Universal ++= Seq(
  // JVM memory tuning
  "-J-Xmx512m",
  "-J-Xms256m",

  // Since play uses separate pidfile we have to provide it with a proper path
  s"-Dpidfile.path=/var/run/${packageName.value}/play.pid",

  // Use separate configuration file for production environment
  s"-Dconfig.file=/usr/share/${packageName.value}/conf/production.conf",

  // Use separate logger configuration file for production environment
  s"-Dlogger.file=/usr/share/${packageName.value}/conf/production-logger.xml",

  // You may also want to include this setting if you use play evolutions
  "-DapplyEvolutions.default=true"
)

daemonUser in Linux := "findmovie"
maintainer in Linux := "First Lastname <first.last@example.com>"
packageSummary in Linux := "My custom package summary"
packageDescription := "My longer package description"
rpmRelease := "6"
rpmVendor := "example.com"
rpmUrl := Some("http://github.com/example/server")
rpmLicense := Some("Apache v2")