USE fm_users;

# --- !Ups
create table USERS (
  id int not null,
  email varchar(255) not null,
  password char(255) not null,

  primary key(id),
  constraint uc_user_email unique (email)
);

create index idx_users_email on USERS(email);

# --- !Downs
drop table USERS;
