package controllers

import javax.inject.Inject

import controllers.utils.JsonValidationError
import models._
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.json.{JsError, Json}
import play.api.mvc._


class Session @Inject() (
  storage: SessionStorage,
  tokenGenerator: TokenGenerator,
  users: UserDAO,
  passwordEncoder: PasswordEncoder,
  authenticator: Authenticator
) extends Controller {

  case class CreateParams(email: String, password: String)
  implicit val createParamsJsonFormat = Json.format[CreateParams]
  def create() = Action.async(parse.json) { req =>
    req.body.validate[CreateParams].map { params =>
      authenticator.validateUser(params.email, params.password).map { session =>
        Ok(Json.toJson(session))
      }.recover {
        case e: AuthenticationException => Forbidden(Json.obj("error" -> "Invalid username or password"))
      }
    }.recoverTotal {
      case e: JsError => throw JsonValidationError(e)
    }
  }

  def get(token: String) = Action {
    val session = authenticator.validateToken(token)
    Ok(Json.toJson(session))
  }

}
