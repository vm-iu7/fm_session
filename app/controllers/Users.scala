package controllers

import com.google.inject.Inject
import controllers.utils.JsonValidationError
import models.{PasswordEncoder, UserDAO}

import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.json.{JsError, Json}
import play.api.mvc.{Action, Controller}


class Users @Inject() (
  users: UserDAO,
  passwordEncoder: PasswordEncoder
) extends Controller {

  case class CreateParams(id: Int, email: String, password: String)
  implicit val createParamsFormat = Json.format[CreateParams]
  def create() = Action.async(parse.json) { req =>
    req.body.validate[CreateParams].map { params =>
      val pass = passwordEncoder.encodePassword(params.password)
      users.create(params.id, params.email, pass).map { user =>
        Ok(Json.toJson(user))
      }
    }.recoverTotal {
      case e: JsError => throw JsonValidationError(e)
    }
  }

  case class UpdateParams(id: Int, email: String, password: String)
  implicit val updateParamsFormat = Json.format[UpdateParams]
  def update() = Action.async(parse.json) { req =>
    ???
  }

}
