package controllers.utils

import play.api.libs.json.Json


case class Error(error: String)
object Error {
  implicit val jsonFormat = Json.format[Error]
}
