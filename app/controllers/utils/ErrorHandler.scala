package controllers.utils

import java.io.{PrintWriter, StringWriter}

import play.api.http.HttpErrorHandler
import play.api.libs.json.{JsError, Json}
import play.api.mvc.Results._
import play.api.mvc._

import scala.concurrent._


class ErrorHandler extends HttpErrorHandler {

  def onClientError(request: RequestHeader, statusCode: Int, message: String) = {
    Future.successful(
      Status(statusCode)(Json.obj("error" -> message))
    )
  }

  def onServerError(request: RequestHeader, exception: Throwable) = {
    Future.successful {
      exception match {
        case e: NoSuchElementException =>
          NotFound(Json.obj("error" -> exception.getMessage))

        case e: ForbiddenException =>
          Forbidden(Json.obj("error" -> e.getMessage))

        case JsonValidationError(jsError) =>
          BadRequest(Json.obj(
            "error" -> "Failed to validate json",
            "details" -> JsError.toJson(jsError)
          ))

        case e =>
          val sw = new StringWriter()
          val pw = new PrintWriter(sw)
          e.printStackTrace(pw)
          InternalServerError(Json.obj(
            "error" -> exception.getMessage,
            "stacktrace" -> sw.toString
          ))
      }
    }
  }
}

case class JsonValidationError(jsError: JsError) extends Exception(jsError.toString)
case class ForbiddenException(msg: String) extends Exception(msg)