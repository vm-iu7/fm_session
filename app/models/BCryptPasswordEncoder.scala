package models

import com.github.t3hnar.bcrypt._


class BCryptPasswordEncoder extends PasswordEncoder {
  override def encodePassword(plainPassword: String): String = {
    plainPassword.bcrypt
  }

  override def validatePassword(password: String, encryptedPassword: String): Boolean = {
    password.isBcrypted(encryptedPassword)
  }
}
