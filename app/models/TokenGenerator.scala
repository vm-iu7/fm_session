package models

import scala.util.Random

import com.google.inject.{ImplementedBy}


@ImplementedBy(classOf[UuidTokenGenerator])
trait TokenGenerator {
  def generate(): String
}

class AlphanumericTokenGenerator extends TokenGenerator {
  def generate(): String = {
    (Random.alphanumeric take 25).mkString("")
  }
}

class UuidTokenGenerator extends TokenGenerator {
  override def generate(): String = java.util.UUID.randomUUID().toString
}
