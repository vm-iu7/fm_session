package models

import java.util.NoSuchElementException

import com.google.inject.Inject
import scala.async.Async._
import scala.concurrent.{ExecutionContext, Future}


class Authenticator @Inject() (
  passwordEncoder: PasswordEncoder,
  users: UserDAO,
  sessionStorage: SessionStorage,
  tokenGenerator: TokenGenerator
) {
  def validateUser(email: String, plainPassword: String)(implicit ec: ExecutionContext): Future[Session] = async {
    val userOpt = await { users.getByEmail(email) }
    userOpt.map { user =>
      if (passwordEncoder.validatePassword(plainPassword, user.password)) {
        val token = tokenGenerator.generate()
        sessionStorage.create(user.id, token)
      } else {
        throw new AuthenticationException("Incorrect password")
      }

    }.getOrElse {
      throw new NoSuchElementException(s"No user with $email email")
    }
  }

  def validateToken(token: String)(implicit ec: ExecutionContext): Session = {
    sessionStorage.validate(token).getOrElse {
      throw new NoSuchElementException(s"Invalid token")
    }
  }
}
