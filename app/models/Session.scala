package models

import play.api.libs.json.Json


case class Session(token: String, userId: Int, ttl: Long)

object Session {
  implicit val jsonForamt = Json.format[Session]
}
