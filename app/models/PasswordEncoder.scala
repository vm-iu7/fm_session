package models

import com.google.inject.ImplementedBy


@ImplementedBy(classOf[BCryptPasswordEncoder])
trait PasswordEncoder {
  def encodePassword(plainPassword: String): String
  def validatePassword(password: String, encryptedPassword: String): Boolean
}
