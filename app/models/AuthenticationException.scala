package models


class AuthenticationException(details: String) extends RuntimeException(details)
