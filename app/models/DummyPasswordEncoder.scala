package models


class DummyPasswordEncoder extends PasswordEncoder {
  def encodePassword(plainPassword: String): String = {
    plainPassword
  }

  def validatePassword(password: String, encryptedPassword: String): Boolean = {
    password == encryptedPassword
  }
}
