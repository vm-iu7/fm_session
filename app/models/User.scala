package models

import javax.inject.{Singleton, Inject}

import play.api.db.slick.{HasDatabaseConfig, DatabaseConfigProvider}
import play.api.libs.json.Json
import slick.driver.JdbcProfile

import scala.concurrent.{ExecutionContext, Future}


case class User(id: Int, email: String, password: String)

object User {
  implicit val jsonFormat = Json.format[User]
}

@Singleton
class UserDAO @Inject() (
  protected val dbConfigProvider: DatabaseConfigProvider
) extends HasDatabaseConfig[JdbcProfile] {

  import driver.api._

  protected val dbConfig = dbConfigProvider.get[JdbcProfile]

  private val usersTable = TableQuery[UsersTable]

  def lookup(id: Int)(implicit ec: ExecutionContext): Future[Option[User]] = {
    dbConfig.db.run {
      usersTable.filter(u => u.id === id).result.headOption
    }
  }

  def getByEmail(email: String)(implicit ec: ExecutionContext): Future[Option[User]] = {
    dbConfig.db.run {
      usersTable.filter(u => u.email === email).result.headOption
    }
  }

  def create(id: Int, email: String, password: String)
    (implicit ec: ExecutionContext): Future[User] = {
    val user = User(id, email, password)
    dbConfig.db.run(usersTable += user).map(_ => user)
  }

  private class UsersTable(tag: Tag) extends Table[User](tag, "USERS") {
    def id = column[Int]("id", O.PrimaryKey)
    def email = column[String]("email")
    def password = column[String]("password")

    def * = (id, email, password) <> ((User.apply _).tupled, User.unapply)
  }

}
