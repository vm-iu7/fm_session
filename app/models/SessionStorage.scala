package models

import com.google.inject.{ImplementedBy, Inject}
import com.redis._
import com.redis.serialization.Parse.Implicits._
import play.api.Configuration


@ImplementedBy(classOf[RedisSessionStorage])
trait SessionStorage {
  def create(userId: Int, token: String): Session
  def validate(token: String): Option[Session]
}


class RedisSessionStorage @Inject() (
  private val config: Configuration
) extends SessionStorage {

  private val redisClient = (for {
    redisHost <- config.getString("redis.host")
    redisPort <- config.getInt("redis.port")
  } yield {
    new RedisClient(redisHost, redisPort)
  }).get

  val tokenTTL = config.getInt("tokenTTL").get

  def create(userId: Int, token: String): Session = {
    redisClient.set(token, userId)
    redisClient.expire(token, tokenTTL)
    Session(token, userId, tokenTTL)
  }

  def validate(token: String): Option[Session] = {
    redisClient.get[Int](token).map { userId =>
      val ttl = redisClient.ttl(token).get
      Session(token, userId, ttl)
    }
  }

}
